package ca.uqac.lif.ecp.lab.fsm;

import java.util.Random;

import ca.uqac.lif.ecp.CoverageMetric;
import ca.uqac.lif.ecp.GreedyTraceGenerator;
import ca.uqac.lif.ecp.atomic.AtomicEvent;
import ca.uqac.lif.ecp.atomic.Automaton;
import ca.uqac.lif.ecp.atomic.GreedyAutomatonGenerator;
import ca.uqac.lif.ecp.lab.CoverageMetricProvider;
import ca.uqac.lif.ecp.lab.GreedyTraceGeneratorProvider;
import ca.uqac.lif.ecp.lab.TestSuiteGenerationExperiment;
import ca.uqac.lif.labpal.Experiment;

public class GreedyAutomatonGeneratorProvider implements GreedyTraceGeneratorProvider<AtomicEvent> 
{
	protected AutomatonProvider m_automatonProvider;
	
	protected CoverageMetricProvider<AtomicEvent,Float> m_metricProvider;
	
	public static final String NAME = "Random";
	
	/**
	 * The maximum number of iterations authorized
	 */
	protected int m_maxIterations = 1000;
	
	public GreedyAutomatonGeneratorProvider(AutomatonProvider ap, CoverageMetricProvider<AtomicEvent,Float> mp)
	{
		super();
		m_automatonProvider = ap;
		m_metricProvider = mp;
	}
	
	public void setMaxIterations(int iterations)
	{
		m_maxIterations = iterations;
	}

	@Override
	public GreedyTraceGenerator<AtomicEvent> getGenerator() 
	{
		Automaton aut = m_automatonProvider.getAutomaton();
		CoverageMetric<AtomicEvent,Float> metric = m_metricProvider.getMetric();
		GreedyAutomatonGenerator gag = new GreedyAutomatonGenerator(aut, new Random(), metric);
		gag.setMaxIterations(m_maxIterations);
		return gag;
	}

	@Override
	public void write(Experiment e) 
	{
		m_automatonProvider.write(e);
		m_metricProvider.write(e);
		e.write(TestSuiteGenerationExperiment.METHOD, NAME);
	}

}
