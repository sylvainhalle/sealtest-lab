/*
    Log trace triaging and etc.
    Copyright (C) 2016 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.uqac.lif.ecp.lab.fsm;

import ca.uqac.lif.ecp.CayleyCategoryCoverage;
import ca.uqac.lif.ecp.CayleyGraph;
import ca.uqac.lif.ecp.CoverageMetric;
import ca.uqac.lif.ecp.TriagingFunction;
import ca.uqac.lif.ecp.atomic.AtomicEvent;
import ca.uqac.lif.ecp.atomic.Automaton;
import ca.uqac.lif.ecp.atomic.AutomatonCayleyGraphFactory;
import ca.uqac.lif.ecp.lab.CoverageMetricProvider;
import ca.uqac.lif.ecp.lab.TriagingFunctionProvider;
import ca.uqac.lif.labpal.Experiment;

public class AutomatonCoverageMetricProvider<U> implements CoverageMetricProvider<AtomicEvent, Float> 
{
	protected AutomatonProvider m_automatonProvider;
	
	protected TriagingFunctionProvider<AtomicEvent,U> m_functionProvider;
	
	public AutomatonCoverageMetricProvider(AutomatonProvider ap, TriagingFunctionProvider<AtomicEvent,U> tfp)
	{
		super();
		m_automatonProvider = ap;
		m_functionProvider = tfp;
	}
	
	@Override
	public CoverageMetric<AtomicEvent,Float> getMetric()
	{
		Automaton aut = m_automatonProvider.getAutomaton();
		AutomatonCayleyGraphFactory<U> factory = new AutomatonCayleyGraphFactory<U>(aut.getAlphabet());
		TriagingFunction<AtomicEvent,U> function = m_functionProvider.getFunction();
		CayleyGraph<AtomicEvent,U> graph = factory.getGraph(function);
		CoverageMetric<AtomicEvent,Float> metric = new CayleyCategoryCoverage<AtomicEvent,U>(graph, function);
		return metric;
	}
	
	@Override
	public void write(Experiment e)
	{
		m_automatonProvider.write(e);
		m_functionProvider.write(e);
	}

}
