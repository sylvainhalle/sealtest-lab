/*
    Log trace triaging and etc.
    Copyright (C) 2016 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.uqac.lif.ecp.lab.pages;

import java.io.InputStream;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;

import ca.uqac.lif.ecp.lab.TestSuiteLab;
import ca.uqac.lif.labpal.FileHelper;
import ca.uqac.lif.labpal.LabAssistant;
import ca.uqac.lif.labpal.Laboratory;
import ca.uqac.lif.labpal.server.CustomPageCallback;

public class LtlCallback extends CustomPageCallback
{
	public LtlCallback(Laboratory lab, LabAssistant assistant)
	{
		super("/data/formulas", lab, assistant);
	}
	
	@Override
	public String fill(String page, Map<String,String> params)
	{
		String out = page.replaceAll("\\{%TITLE%\\}", "LTL properties");
		out = out.replaceAll("\\{%FAVICON%\\}", getFavicon(IconType.GRAPH));
		String page_contents = FileHelper.internalFileToString(this.getClass(), "formulas-list.html");
		page_contents = page_contents.replaceAll("\\{%FSM_LIST%\\}", Matcher.quoteReplacement(createLtlList().toString()));
		out = out.replaceAll("\\{%CONTENT%\\}", Matcher.quoteReplacement(page_contents.toString()));
		return out;
	}
	
	public StringBuilder createLtlList()
	{
		StringBuilder out = new StringBuilder();
		out.append("<ul>\n");
		InputStream is = FileHelper.internalFileToStream(TestSuiteLab.class, TestSuiteLab.s_ltlPath + "dwyer-ltl.txt");
		Scanner scanner = new Scanner(is);
		while (scanner.hasNextLine())
		{
			String line = scanner.nextLine().trim();
			if (line.isEmpty() || line.startsWith("#"))
			{
				continue;
			}
			out.append("<li>").append(htmlEscape(line)).append("</li>\n");
		}
		out.append("</ul>");
		scanner.close();
		return out;
	}

}
