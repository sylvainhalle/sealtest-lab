/*
    Log trace triaging and etc.
    Copyright (C) 2016-2017 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.uqac.lif.ecp.lab.statechart;

import ca.uqac.lif.ecp.atomic.AtomicEvent;
import ca.uqac.lif.ecp.ltl.And;
import ca.uqac.lif.ecp.ltl.Equals;
import ca.uqac.lif.ecp.ltl.LessThan;
import ca.uqac.lif.ecp.ltl.Not;
import ca.uqac.lif.ecp.statechart.Configuration;
import ca.uqac.lif.ecp.statechart.DecrementVariableBy;
import ca.uqac.lif.ecp.statechart.InState;
import ca.uqac.lif.ecp.statechart.IncrementVariableBy;
import ca.uqac.lif.ecp.statechart.NestedState;
import ca.uqac.lif.ecp.statechart.SetVariableTo;
import ca.uqac.lif.ecp.statechart.State;
import ca.uqac.lif.ecp.statechart.Statechart;import ca.uqac.lif.ecp.statechart.StatechartVariableAtom;
import ca.uqac.lif.ecp.statechart.atomic.AtomicStatechart;
import ca.uqac.lif.ecp.statechart.atomic.AtomicStatechartRenderer;
import ca.uqac.lif.ecp.statechart.atomic.AtomicTransition;

/**
 * UML statechart of the "Autoradio" property, from this
 * <a href="http://dx.doi.org/10.14279/depositonce-1778">PhD thesis</a>.
 * @author Sylvain Hallé
 *
 */
public class Autoradio implements StatechartProvider<AtomicEvent>
{
	/**
	 * The instance of the statechart
	 */
	private final AtomicStatechart m_statechart;

	/**
	 * The maximum number of tracks in the CD
	 */
	protected final int m_maxTracks;

	/**
	 * Creates an instance of the Autoradio statechart
	 * @param max_tracks The maximum number of tracks in the CD
	 */
	public Autoradio(int max_tracks)
	{
		super();
		m_maxTracks = max_tracks;
		m_statechart = createStatechart(max_tracks);
	}

	@Override
	public Statechart<AtomicEvent> getStatechart() 
	{
		return m_statechart;
	}

	private static AtomicStatechart createStatechart(int max_tracks)
	{
		// Create events
		AtomicEvent e_O = new AtomicEvent("O");
		AtomicEvent e_Src = new AtomicEvent("Src");
		AtomicEvent e_TapeEject = new AtomicEvent("TapeEject");
		AtomicEvent e_CDEject = new AtomicEvent("CDEject");
		AtomicEvent e_Next = new AtomicEvent("Next");
		AtomicEvent e_Back = new AtomicEvent("Back");
		AtomicEvent e_EndOfTape = new AtomicEvent("EndOfTape");
		AtomicEvent e_Ready = new AtomicEvent("Ready");
		AtomicEvent e_EndOfTitle = new AtomicEvent("EndOfTitle");
		AtomicEvent e_CDIn = new AtomicEvent("CDIn");
		AtomicEvent e_NewTrack = new AtomicEvent("NewTrack");
		AtomicEvent e_LastTrack = new AtomicEvent("LastTrack");
		AtomicEvent e_TapeIn = new AtomicEvent("TapeIn");
		AtomicEvent e_TapeOut = new AtomicEvent("TapeOut");
		AtomicStatechart full_sc = new AtomicStatechart();
		// Declare state variables
		full_sc.setVariable("T", 0);
		full_sc.setVariable("T_A", 1);
		NestedState<AtomicEvent> car_audio_system = new NestedState<AtomicEvent>("CarAudioSystem");
		{
			AtomicStatechart audio_player = new AtomicStatechart();
			car_audio_system.addStatechart(audio_player);
			audio_player.add(new State<AtomicEvent>("Off"));
			{
				// "On" ntested state
				NestedState<AtomicEvent> audio_ctrl_box = new NestedState<AtomicEvent>("On");
				AtomicStatechart audio_ctrl = new AtomicStatechart();
				{
					// Tuner Mode nested state
					NestedState<AtomicEvent> tuner_mode = new NestedState<AtomicEvent>("TunerMode");
					AtomicStatechart tuner_ctrl = new AtomicStatechart();
					tuner_ctrl.add(new State<AtomicEvent>("1"));
					tuner_ctrl.add(new State<AtomicEvent>("2"));
					tuner_ctrl.add(new State<AtomicEvent>("3"));
					tuner_ctrl.add(new State<AtomicEvent>("4"));
					tuner_ctrl.add("1", new AtomicTransition(e_Next, new Configuration<AtomicEvent>("2")));
					tuner_ctrl.add("2", new AtomicTransition(e_Next, new Configuration<AtomicEvent>("3")));
					tuner_ctrl.add("3", new AtomicTransition(e_Next, new Configuration<AtomicEvent>("4")));
					tuner_ctrl.add("4", new AtomicTransition(e_Next, new Configuration<AtomicEvent>("1")));
					tuner_ctrl.add("1", new AtomicTransition(e_Back, new Configuration<AtomicEvent>("4")));
					tuner_ctrl.add("2", new AtomicTransition(e_Back, new Configuration<AtomicEvent>("1")));
					tuner_ctrl.add("3", new AtomicTransition(e_Back, new Configuration<AtomicEvent>("2")));
					tuner_ctrl.add("4", new AtomicTransition(e_Back, new Configuration<AtomicEvent>("3")));
					tuner_mode.addStatechart(tuner_ctrl);
					audio_ctrl.add(tuner_mode);
				}
				{
					NestedState<AtomicEvent> tape_mode = new NestedState<AtomicEvent>("TapeMode");
					AtomicStatechart tape_ctrl = new AtomicStatechart();
					tape_ctrl.add(new State<AtomicEvent>("Playing"));
					tape_ctrl.add(new State<AtomicEvent>("Backward Spooling"));
					tape_ctrl.add(new State<AtomicEvent>("Forward Spooling"));
					tape_ctrl.add("Playing", new AtomicTransition(e_Next, new Configuration<AtomicEvent>("Forward Spooling")));
					tape_ctrl.add("Playing", new AtomicTransition(e_Back, new Configuration<AtomicEvent>("Backward Spooling")));
					tape_ctrl.add("Forward Spooling", new AtomicTransition(e_Back, new Configuration<AtomicEvent>("Playing")));
					tape_ctrl.add("Backward Spooling", new AtomicTransition(e_Next, new Configuration<AtomicEvent>("Playing")));
					tape_ctrl.add("Backward Spooling", new AtomicTransition(e_EndOfTape, new Configuration<AtomicEvent>("Playing")));
					tape_mode.addStatechart(tape_ctrl);
					audio_ctrl.add(tape_mode);
				}
				{
					NestedState<AtomicEvent> cd_mode = new NestedState<AtomicEvent>("CDMode");
					AtomicStatechart cd_ctrl = new AtomicStatechart();
					cd_ctrl.add(new State<AtomicEvent>("CD Playing"));
					cd_ctrl.add(new State<AtomicEvent>("Selecting Next Track"));
					cd_ctrl.add(new State<AtomicEvent>("Selecting Previous Track"));
					cd_ctrl.add("CD Playing", new AtomicTransition(e_Next, new Configuration<AtomicEvent>("Selecting Next Track")));
					cd_ctrl.add("CD Playing", new AtomicTransition(e_Back, new Configuration<AtomicEvent>("Selecting Previous Track")));
					{
						AtomicTransition t = new AtomicTransition(e_EndOfTitle, new Configuration<AtomicEvent>("Selecting Next Track"));
						t.setGuard(new LessThan<AtomicEvent>(
								new StatechartVariableAtom<AtomicEvent,Number>("T_A", full_sc),	
								new StatechartVariableAtom<AtomicEvent,Number>("T", full_sc)
								));
						cd_ctrl.add("CD Playing", t);
					}
					{
						AtomicTransition t = new AtomicTransition(e_Ready, new Configuration<AtomicEvent>("CD Playing"));
						t.setAction(new IncrementVariableBy<AtomicEvent>("T_A", 1, max_tracks));
						cd_ctrl.add("Selecting Next Track", t);
					}
					{
						AtomicTransition t = new AtomicTransition(e_Ready, new Configuration<AtomicEvent>("CD Playing"));
						t.setAction(new DecrementVariableBy<AtomicEvent>("T_A", 1, 0));
						cd_ctrl.add("Selecting Previous Track", t);
					}
					cd_mode.addStatechart(cd_ctrl);
					audio_ctrl.add(cd_mode);
				}
				audio_ctrl_box.addStatechart(audio_ctrl);
				audio_player.add(audio_ctrl_box);
				audio_player.add("Off", new AtomicTransition(e_O, new Configuration<AtomicEvent>("On")));
				audio_player.add("On", new AtomicTransition(e_O, new Configuration<AtomicEvent>("Off")));
				{
					AtomicTransition t = new AtomicTransition(e_Src, new Configuration<AtomicEvent>("TapeMode"));
					t.setGuard(new InState<AtomicEvent>(full_sc, "TapeFull"));
					audio_ctrl.add("TunerMode", t);
				}
				{
					AtomicTransition t = new AtomicTransition(e_Src, new Configuration<AtomicEvent>("CDMode"));
					t.setGuard(new And<AtomicEvent>(
							new InState<AtomicEvent>(full_sc, "CDFull"),
							new Not<AtomicEvent>(new InState<AtomicEvent>(full_sc, "TapeFull"))));
					audio_ctrl.add("TunerMode", t);
				}
				{
					AtomicTransition t = new AtomicTransition(e_Src, new Configuration<AtomicEvent>("TunerMode"));
					t.setGuard(new Not<AtomicEvent>(new InState<AtomicEvent>(full_sc, "CDFull")));
					audio_ctrl.add("TapeMode", t);
				}
				{
					AtomicTransition t = new AtomicTransition(e_TapeEject, new Configuration<AtomicEvent>("TunerMode"));
					t.setGuard(new InState<AtomicEvent>(full_sc, "CDEmpty"));
					audio_ctrl.add("TapeMode", t);
				}
				{
					AtomicTransition t = new AtomicTransition(e_Src, new Configuration<AtomicEvent>("CDMode"));
					t.setGuard(new InState<AtomicEvent>(full_sc, "CDFull"));
					audio_ctrl.add("TapeMode", t);
				}
				{
					AtomicTransition t = new AtomicTransition(e_TapeEject, new Configuration<AtomicEvent>("CDMode"));
					t.setGuard(new InState<AtomicEvent>(full_sc, "CDFull"));
					audio_ctrl.add("TapeMode", t);
				}
				audio_ctrl.add("CDMode", new AtomicTransition(e_Src, new Configuration<AtomicEvent>("TunerMode")));
				audio_ctrl.add("CDMode", new AtomicTransition(e_CDEject, new Configuration<AtomicEvent>("TunerMode")));
				{
					AtomicTransition t = new AtomicTransition(e_TapeEject, new Configuration<AtomicEvent>("TunerMode"));
					t.setGuard(new Equals<AtomicEvent,Integer>(
							new StatechartVariableAtom<AtomicEvent,Integer>("T_A", full_sc),
							new StatechartVariableAtom<AtomicEvent,Integer>("T", full_sc)
							));
					audio_ctrl.add("CDMode", t);
				}
			}
			AtomicStatechart cd_player = new AtomicStatechart();
			car_audio_system.addStatechart(cd_player);
			{
				cd_player.add(new State<AtomicEvent>("CDEmpty"));
				cd_player.add(new State<AtomicEvent>("CDFull"));
				cd_player.add(new State<AtomicEvent>("ReadTracks"));
				cd_player.add("CDEmpty", new AtomicTransition(e_CDIn, new Configuration<AtomicEvent>("ReadTracks")));
				{
					AtomicTransition t = new AtomicTransition(e_NewTrack, new Configuration<AtomicEvent>("ReadTracks"));
					t.setAction(new IncrementVariableBy<AtomicEvent>("T", 1, max_tracks));
					cd_player.add("ReadTracks", t);
				}
				{
					AtomicTransition t = new AtomicTransition(e_LastTrack, new Configuration<AtomicEvent>("CDFull"));
					t.setAction(new IncrementVariableBy<AtomicEvent>("T", 1, max_tracks));
					cd_player.add("ReadTracks", t);
				}
				{
					AtomicTransition t = new AtomicTransition(e_CDEject, new Configuration<AtomicEvent>("CDEmpty"));
					t.setGuard(new InState<AtomicEvent>(full_sc, "On"));
					t.setAction(new SetVariableTo<AtomicEvent,Integer>("T", 0));
					cd_player.add("CDFull", t);
				}
			}
			AtomicStatechart tape_deck = new AtomicStatechart();
			car_audio_system.addStatechart(tape_deck);
			{
				tape_deck.add(new State<AtomicEvent>("TapeEmpty"));
				tape_deck.add(new State<AtomicEvent>("TapeFull"));
				tape_deck.add("TapeEmpty", new AtomicTransition(e_TapeIn, new Configuration<AtomicEvent>("TapeFull")));
				tape_deck.add("TapeFull", new AtomicTransition(e_TapeOut, new Configuration<AtomicEvent>("TapeEmpty")));
			}
		}
		full_sc.add(car_audio_system);
		return full_sc;
	}

	public static void main(String[] args)
	{
		// Simple main just to display the statechart
		Autoradio ar = new Autoradio(2);
		Statechart<AtomicEvent> sc = ar.getStatechart();
		System.out.println(AtomicStatechartRenderer.toDot(sc));
	}

}
