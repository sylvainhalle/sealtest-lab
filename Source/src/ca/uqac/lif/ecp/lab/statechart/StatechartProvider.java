package ca.uqac.lif.ecp.lab.statechart;

import ca.uqac.lif.ecp.Event;
import ca.uqac.lif.ecp.statechart.Statechart;

public interface StatechartProvider<T extends Event>
{
	public Statechart<T> getStatechart();
}
