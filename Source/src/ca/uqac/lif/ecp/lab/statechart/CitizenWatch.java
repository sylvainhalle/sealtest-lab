/*
    Log trace triaging and etc.
    Copyright (C) 2016-2017 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.uqac.lif.ecp.lab.statechart;

import ca.uqac.lif.ecp.atomic.AtomicEvent;
import ca.uqac.lif.ecp.statechart.Configuration;
import ca.uqac.lif.ecp.statechart.InState;
import ca.uqac.lif.ecp.statechart.NestedState;
import ca.uqac.lif.ecp.statechart.State;
import ca.uqac.lif.ecp.statechart.Statechart;
import ca.uqac.lif.ecp.statechart.atomic.AtomicStatechart;
import ca.uqac.lif.ecp.statechart.atomic.AtomicStatechartRenderer;
import ca.uqac.lif.ecp.statechart.atomic.AtomicTransition;

/**
 * UML statechart of Harel's Citizen Watch.
 * <a href="http://dx.doi.org/10.14279/depositonce-1778">PhD thesis</a>.
 * @author Sylvain Hallé
 *
 */
public class CitizenWatch implements StatechartProvider<AtomicEvent>
{
	/**
	 * The instance of the statechart
	 */
	private final AtomicStatechart m_statechart;

	/**
	 * Creates an instance of the Citizen Watch statechart
	 */
	public CitizenWatch()
	{
		super();
		m_statechart = createStatechart();
	}

	@Override
	public Statechart<AtomicEvent> getStatechart() 
	{
		return m_statechart;
	}

	private static AtomicStatechart createStatechart()
	{
		// Create events
		AtomicEvent e_battery_inserted = new AtomicEvent("battery inserted");
		AtomicEvent e_battery_removed = new AtomicEvent("battery removed");
		AtomicEvent e_b = new AtomicEvent("b");
		AtomicEvent e_b_hat = new AtomicEvent("b^");
		AtomicEvent e_d = new AtomicEvent("d");
		AtomicEvent e_d_hat = new AtomicEvent("d^");
		AtomicEvent e_battery_weakens = new AtomicEvent("battery weakens");
		AtomicEvent e_battery_dies = new AtomicEvent("weak battery dies");
		AtomicEvent e_T_whole_hour = new AtomicEvent("T is whole hour.");
		AtomicEvent e_2_sec = new AtomicEvent("2 sec.");
		// Create statechart
		AtomicStatechart citizen_quartz_multi_alarm = new AtomicStatechart();
		citizen_quartz_multi_alarm.add(new State<AtomicEvent>("dead"));
		NestedState<AtomicEvent> main = new NestedState<AtomicEvent>("main");
		{
			{
				// Light
				AtomicStatechart light = new AtomicStatechart("light");
				light.add(new State<AtomicEvent>("off"));
				light.add(new State<AtomicEvent>("on"));
				light.add("off", new AtomicTransition(e_b, new Configuration<AtomicEvent>("on")));
				light.add("on", new AtomicTransition(e_b_hat, new Configuration<AtomicEvent>("off")));
				main.addStatechart(light);
			}
			{
				// Power
				AtomicStatechart power = new AtomicStatechart("power");
				power.add(new State<AtomicEvent>("OK"));
				power.add(new State<AtomicEvent>("blink"));
				power.add("OK", new AtomicTransition(e_battery_weakens, new Configuration<AtomicEvent>("blink")));
				main.addStatechart(power);
			}
			{
				// Chime Status
				AtomicStatechart chime = new AtomicStatechart("chime-status");
				chime.add(new State<AtomicEvent>("disabled"));
				NestedState<AtomicEvent> enabled = new NestedState<AtomicEvent>("enabled");
				{
					AtomicStatechart en_sc = new AtomicStatechart();
					en_sc.add(new State<AtomicEvent>("quiet"));
					en_sc.add(new State<AtomicEvent>("beep"));
					en_sc.add("quiet", new AtomicTransition(e_T_whole_hour, new Configuration<AtomicEvent>("beep")));
					en_sc.add("beep", new AtomicTransition(e_2_sec, new Configuration<AtomicEvent>("quiet")));
					enabled.addStatechart(en_sc);
					chime.add(enabled);
				}
				{
					AtomicTransition t = new AtomicTransition(e_d, new Configuration<AtomicEvent>("disabled"));
					t.setGuard(new InState<AtomicEvent>(citizen_quartz_multi_alarm, "chime on"));
					chime.add("enabled", t);
				}
				{
					AtomicTransition t = new AtomicTransition(e_d, new Configuration<AtomicEvent>("enabled"));
					t.setGuard(new InState<AtomicEvent>(citizen_quartz_multi_alarm, "chime off"));
					chime.add("disabled", t);
				}
				main.addStatechart(chime);
			}
			{
				// Displays
				AtomicStatechart displays = new AtomicStatechart("displays");
				// Pas terminé!
				main.addStatechart(displays);
			}
		}
		citizen_quartz_multi_alarm.add(main);
		citizen_quartz_multi_alarm.add("dead", new AtomicTransition(e_battery_inserted, new Configuration<AtomicEvent>("main")));
		citizen_quartz_multi_alarm.add("main", new AtomicTransition(e_battery_removed, new Configuration<AtomicEvent>("dead")));
		citizen_quartz_multi_alarm.add("main", new AtomicTransition(e_battery_dies, new Configuration<AtomicEvent>("dead")));
		return citizen_quartz_multi_alarm;
	}

	public static void main(String[] args)
	{
		// Simple main just to display the statechart
		CitizenWatch ar = new CitizenWatch();
		Statechart<AtomicEvent> sc = ar.getStatechart();
		System.out.println(AtomicStatechartRenderer.toDot(sc));
	}


}
