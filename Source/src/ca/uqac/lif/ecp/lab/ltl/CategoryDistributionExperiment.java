package ca.uqac.lif.ecp.lab.ltl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ca.uqac.lif.ecp.CayleyGraph;
import ca.uqac.lif.ecp.atomic.AtomicEvent;
import ca.uqac.lif.ecp.lab.CayleyGraphProvider;
import ca.uqac.lif.ecp.ltl.Operator;
import ca.uqac.lif.json.JsonList;
import ca.uqac.lif.json.JsonNumber;
import ca.uqac.lif.labpal.Experiment;
import ca.uqac.lif.labpal.ExperimentException;
import ca.uqac.lif.labpal.Formatter;
import ca.uqac.lif.structures.MathSet;

public class CategoryDistributionExperiment extends Experiment 
{
	protected final CayleyGraphProvider<AtomicEvent,Operator<AtomicEvent>> m_provider;
	
	protected static int max_len = 20;
	
	public CategoryDistributionExperiment(CayleyGraphProvider<AtomicEvent,Operator<AtomicEvent>> provider)
	{
		super();
		m_provider = provider;
		setDescription("Plots the cardinality of each equivalence class for a single triaging function based on holograms.");
		describe("Cardinality", "The cardinality of the equivalence class");
		describe("Number", "The number of categories with that cardinality");
		write("Cardinality", new JsonList());
		write("Number", new JsonList());
	}

	@Override
	public void execute() throws ExperimentException 
	{
		CayleyGraph<AtomicEvent,Operator<AtomicEvent>> graph = m_provider.getCayleyGraph();
		Map<MathSet<Operator<AtomicEvent>>,Integer> map = graph.getClassCardinality(max_len, true);
		Map<Integer,Integer> card_map = new HashMap<Integer,Integer>();
		for (int card : map.values())
		{
			if (!card_map.containsKey(card))
			{
				card_map.put(card,  1);
			}
			else
			{
				int val = card_map.get(card);
				card_map.put(card,  val + 1);
			}
		}
		JsonList c = new JsonList();
		JsonList n = new JsonList();
		List<Integer> cards = new ArrayList<Integer>(card_map.size());
		cards.addAll(card_map.keySet());
		Collections.sort(cards);
		long total = 0;
		for (int cardinality : cards)
		{
			total += cardinality;
		}
		float cumul_card = 0, cumul_n = 0;
		for (int cardinality : cards)
		{
			cumul_card += Formatter.divide(cardinality, total);
			cumul_n += card_map.get(cardinality);
			c.add(new JsonNumber(cumul_card));
			n.add(new JsonNumber(cumul_n));
		}
		write("Cardinality", c);
		write("Number", n);
	}

}
