/*
    Log trace triaging and etc.
    Copyright (C) 2016 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.uqac.lif.ecp.lab.ltl;

import ca.uqac.lif.ecp.CayleyCategoryCoverage;
import ca.uqac.lif.ecp.CayleyGraph;
import ca.uqac.lif.ecp.CoverageMetric;
import ca.uqac.lif.ecp.atomic.AtomicEvent;
import ca.uqac.lif.ecp.lab.CoverageMetricProvider;
import ca.uqac.lif.ecp.ltl.AtomicLtlCayleyGraphFactory;
import ca.uqac.lif.ecp.ltl.HologramFunction;
import ca.uqac.lif.ecp.ltl.Operator;
import ca.uqac.lif.labpal.Experiment;

public class HologramCoverageMetricProvider<U> implements CoverageMetricProvider<AtomicEvent, Float> 
{
	protected HologramTriagingFunctionProvider m_functionProvider;
	
	public HologramCoverageMetricProvider(HologramTriagingFunctionProvider tfp)
	{
		super();
		m_functionProvider = tfp;
	}
	
	@Override
	public CoverageMetric<AtomicEvent,Float> getMetric()
	{
		HologramFunction<AtomicEvent> function = m_functionProvider.getFunction();
		AtomicLtlCayleyGraphFactory factory = new AtomicLtlCayleyGraphFactory();
		CayleyGraph<AtomicEvent,Operator<AtomicEvent>> graph = factory.getGraph(function);
		CoverageMetric<AtomicEvent,Float> metric = new CayleyCategoryCoverage<AtomicEvent,Operator<AtomicEvent>>(graph, function);
		return metric;
	}
	
	@Override
	public void write(Experiment e)
	{
		m_functionProvider.write(e);
		m_functionProvider.write(e);
	}

}
