/*
    Log trace triaging and etc.
    Copyright (C) 2016 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.uqac.lif.ecp.lab.ltl;

import java.util.Set;

import ca.uqac.lif.ecp.atomic.AtomicEvent;
import ca.uqac.lif.ecp.lab.TriagingFunctionProvider;
import ca.uqac.lif.ecp.ltl.HologramFunction;
import ca.uqac.lif.ecp.ltl.HologramTransformation;
import ca.uqac.lif.ecp.ltl.Operator;
import ca.uqac.lif.labpal.Experiment;

public class HologramTriagingFunctionProvider implements TriagingFunctionProvider<AtomicEvent,Operator<AtomicEvent>> 
{
	protected HologramTransformationProvider<AtomicEvent> m_transformationProvider;
	
	protected OperatorProvider<AtomicEvent> m_operatorProvider;
	
	protected Set<AtomicEvent> m_alphabet = null;
	
	public HologramTriagingFunctionProvider(OperatorProvider<AtomicEvent> op, HologramTransformationProvider<AtomicEvent> hp)
	{
		super();
		m_operatorProvider = op;
		m_transformationProvider = hp;
	}
	
	@Override
	public HologramFunction<AtomicEvent> getFunction()
	{
		HologramTransformation<AtomicEvent> transformation = m_transformationProvider.getTransformation();
		Operator<AtomicEvent> operator = m_operatorProvider.getOperator();
		HologramFunction<AtomicEvent> function = new HologramFunction<AtomicEvent>(operator, transformation);
		return function;
	}
	
	@Override
	public void write(Experiment e) 
	{
		m_transformationProvider.write(e);
		m_operatorProvider.write(e);
	}
}
