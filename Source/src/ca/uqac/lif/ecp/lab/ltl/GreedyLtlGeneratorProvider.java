package ca.uqac.lif.ecp.lab.ltl;

import java.util.Random;
import java.util.Set;

import ca.uqac.lif.ecp.Alphabet;
import ca.uqac.lif.ecp.CoverageMetric;
import ca.uqac.lif.ecp.GreedyTraceGenerator;
import ca.uqac.lif.ecp.atomic.AtomicEvent;
import ca.uqac.lif.ecp.lab.CoverageMetricProvider;
import ca.uqac.lif.ecp.lab.GreedyTraceGeneratorProvider;
import ca.uqac.lif.ecp.lab.TestSuiteGenerationExperiment;
import ca.uqac.lif.ecp.lab.fsm.GreedyAutomatonGeneratorProvider;
import ca.uqac.lif.ecp.ltl.Atom;
import ca.uqac.lif.ecp.ltl.GreedyLtlGenerator;
import ca.uqac.lif.ecp.ltl.Operator;
import ca.uqac.lif.labpal.Experiment;

public class GreedyLtlGeneratorProvider implements GreedyTraceGeneratorProvider<AtomicEvent> 
{
	protected OperatorProvider<AtomicEvent> m_operatorProvider;
	
	protected CoverageMetricProvider<AtomicEvent,Float> m_metricProvider;
	
	/**
	 * The maximum number of iterations authorized
	 */
	protected int m_maxIterations = 1000;
	
	public GreedyLtlGeneratorProvider(OperatorProvider<AtomicEvent> ap, CoverageMetricProvider<AtomicEvent,Float> mp)
	{
		super();
		m_operatorProvider = ap;
		m_metricProvider = mp;
	}
	
	public void setMaxIterations(int iterations)
	{
		m_maxIterations = iterations;
	}

	@Override
	public GreedyTraceGenerator<AtomicEvent> getGenerator() 
	{
		Operator<AtomicEvent> aut = m_operatorProvider.getOperator();
		CoverageMetric<AtomicEvent,Float> metric = m_metricProvider.getMetric();
		Alphabet<AtomicEvent> alph = new Alphabet<AtomicEvent>();
		fetchAlphabet(aut, alph);
		GreedyLtlGenerator gag = new GreedyLtlGenerator(aut, alph, new Random(), metric);
		gag.setMaxIterations(m_maxIterations);
		return gag;
	}
	
	protected void fetchAlphabet(Operator<AtomicEvent> op, Set<AtomicEvent> alphabet)
	{
		if (op instanceof Atom)
		{
			Atom<AtomicEvent> a = (Atom<AtomicEvent>) op;
			alphabet.add(a.getEvent());
			return;
		}
		for (Operator<AtomicEvent> c_op : op.getTreeChildren())
		{
				fetchAlphabet(c_op, alphabet);
		}
		return;
	}


	@Override
	public void write(Experiment e) 
	{
		m_operatorProvider.write(e);
		m_metricProvider.write(e);
		e.write(TestSuiteGenerationExperiment.METHOD, GreedyAutomatonGeneratorProvider.NAME);
	}

}
