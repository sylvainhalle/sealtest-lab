package ca.uqac.lif.ecp.lab;

import ca.uqac.lif.labpal.ExperimentException;

public class GreedyTestSuiteGenerationExperiment extends LiveGenerationExperiment
{

	public GreedyTestSuiteGenerationExperiment(TestSuiteProvider<?> provider) 
	{
		super(provider);
	}

	@Override
	public void execute() throws ExperimentException 
	{
		super.execute();
		float coverage = m_provider.getCoverage();
		write(TestSuiteGenerationExperiment.COVERAGE, coverage);
	}

}
