/*
    Log trace triaging and etc.
    Copyright (C) 2016 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.uqac.lif.ecp.lab;

import ca.uqac.lif.ecp.Event;
import ca.uqac.lif.ecp.GreedyTraceGenerator;
import ca.uqac.lif.ecp.TestSuite;
import ca.uqac.lif.labpal.Experiment;

/**
 * Generates a test suite from a greedy trace generator.
 * @author Sylvain Hallé
 *
 * @param <T> The type of events
 */
public class GreedyTestSuiteGenerator<T extends Event> implements TestSuiteProvider<T>
{
	/**
	 * The generator provider to obtain a greedy trace generator
	 */
	protected GreedyTraceGeneratorProvider<T> m_generatorProvider;
	
	/**
	 * The generator generated (!) by the provider
	 */
	protected GreedyTraceGenerator<T> m_generator = null;
	
	/**
	 * Creates a new test suite provider
	 * @param graph_provider The graph provider to obtain the Cayley graph
	 * @param generator_provider The generator provider to obtain a Cayley graph
	 *    trace generator
	 */
	public GreedyTestSuiteGenerator(GreedyTraceGeneratorProvider<T> generator_provider)
	{
		super();
		m_generatorProvider = generator_provider;
	}
	
	@Override
	public TestSuite<T> getTestSuite() 
	{
		m_generator = m_generatorProvider.getGenerator();
		return m_generator.generateTraces();
	}

	@Override
	public void write(Experiment e)
	{
		m_generatorProvider.write(e);
	}

	@Override
	public float getCoverage()
	{
		if (m_generator == null)
		{
			return 0f;
		}
		float coverage = m_generator.getLastCoverage();
		return coverage;
	}
}
