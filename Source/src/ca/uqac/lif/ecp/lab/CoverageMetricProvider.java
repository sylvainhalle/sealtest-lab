package ca.uqac.lif.ecp.lab;

import ca.uqac.lif.ecp.CoverageMetric;
import ca.uqac.lif.ecp.Event;
import ca.uqac.lif.labpal.Experiment;

public interface CoverageMetricProvider<T extends Event,U>
{
	public CoverageMetric<T,U> getMetric();
	
	public void write(Experiment e);
}
