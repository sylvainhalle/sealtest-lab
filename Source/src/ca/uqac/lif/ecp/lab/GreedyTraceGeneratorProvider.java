package ca.uqac.lif.ecp.lab;

import ca.uqac.lif.ecp.Event;
import ca.uqac.lif.ecp.GreedyTraceGenerator;
import ca.uqac.lif.labpal.Experiment;

/**
 * Provides a greedy trace generator
 * @author Sylvain
 *
 * @param <T>
 */
public interface GreedyTraceGeneratorProvider<T extends Event>
{
	public GreedyTraceGenerator<T> getGenerator();
	
	public void write(Experiment e);
}
