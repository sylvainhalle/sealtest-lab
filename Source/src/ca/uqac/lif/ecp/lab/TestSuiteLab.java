/*
    Log trace triaging and etc.
    Copyright (C) 2016 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.uqac.lif.ecp.lab;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import ca.uqac.lif.ecp.Edge;
import ca.uqac.lif.ecp.atomic.AtomicEvent;
import ca.uqac.lif.ecp.lab.fsm.AutomatonCoverageMetricProvider;
import ca.uqac.lif.ecp.lab.fsm.AutomatonParser;
import ca.uqac.lif.ecp.lab.fsm.AutomatonProvider;
import ca.uqac.lif.ecp.lab.fsm.CombinatorialTriagingFunctionProvider;
import ca.uqac.lif.ecp.lab.fsm.GreedyAutomatonGeneratorProvider;
import ca.uqac.lif.ecp.lab.fsm.StateHistoryProvider;
import ca.uqac.lif.ecp.lab.fsm.TransitionHistoryProvider;
import ca.uqac.lif.ecp.lab.ltl.CategoryDistributionExperiment;
import ca.uqac.lif.ecp.lab.ltl.GreedyLtlGeneratorProvider;
import ca.uqac.lif.ecp.lab.ltl.HologramCoverageMetricProvider;
import ca.uqac.lif.ecp.lab.ltl.HologramTransformationProvider;
import ca.uqac.lif.ecp.lab.ltl.HologramTriagingFunctionProvider;
import ca.uqac.lif.ecp.lab.ltl.OperatorProvider;
import ca.uqac.lif.ecp.lab.ltl.StringOperatorProvider;
import ca.uqac.lif.ecp.lab.ltl.StringTransformationProvider;
import ca.uqac.lif.ecp.lab.pages.FsmCallback;
import ca.uqac.lif.ecp.lab.pages.GetAutomatonCallback;
import ca.uqac.lif.ecp.lab.pages.LtlCallback;
import ca.uqac.lif.ecp.lab.pages.ShowAutomatonCallback;
import ca.uqac.lif.ecp.ltl.AtomicLtlCayleyGraphFactory;
import ca.uqac.lif.ecp.ltl.Operator;
import ca.uqac.lif.labpal.CliParser;
import ca.uqac.lif.labpal.CliParser.Argument;
import ca.uqac.lif.labpal.CliParser.ArgumentMap;
import ca.uqac.lif.labpal.FileHelper;
import ca.uqac.lif.labpal.Group;
import ca.uqac.lif.labpal.ExperimentBuilder;
import ca.uqac.lif.labpal.ExperimentBuilder.ParseException;
import ca.uqac.lif.labpal.plot.TwoDimensionalPlot.Axis;
import ca.uqac.lif.labpal.plot.gnuplot.GnuBoxPlot;
import ca.uqac.lif.labpal.plot.gnuplot.Scatterplot;
import ca.uqac.lif.labpal.server.WebCallback;
import ca.uqac.lif.labpal.table.BoxTransformation;
import ca.uqac.lif.labpal.table.Composition;
import ca.uqac.lif.labpal.table.ExperimentTable;
import ca.uqac.lif.labpal.table.GroupInColumns;
import ca.uqac.lif.labpal.table.Join;
import ca.uqac.lif.labpal.table.TransformedTable;
import ca.uqac.lif.labpal.table.VersusTable;
import ca.uqac.lif.labpal.Laboratory;
import ca.uqac.lif.structures.MathList;

/**
 * Main class setting up the laboratory
 * 
 * @author Sylvain Hallé
 */
public class TestSuiteLab extends Laboratory
{
	/**
	 * The path where data will be fetched from
	 */
	public static transient final String s_dataPath = "data/";

	/**
	 * The path where write-in experiments will be fetched from
	 */
	public static transient final String s_writeInPath = "ca/uqac/lif/ecp/lab/data/related/";

	/**
	 * The path where FSMs will be read from
	 */
	public static transient final String s_fsmPath = "ca/uqac/lif/ecp/lab/data/fsm/";

	/**
	 * The path where LTL formulas will be read from
	 */
	public static transient final String s_ltlPath = "data/ltl/";

	/**
	 * The minimum value of "t" used in our experiments
	 */
	public int m_minT = 1;

	/**
	 * The maximum value of "t" used in our experiments
	 */
	public int m_maxT = 6;

	/**
	 * The maximum number of iterations before the greedy generator gives up
	 */
	public int m_maxIterations = 5000;

	/**
	 * The number of automata included in the lab
	 */
	public int m_numAutomata = 0;

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		initialize(args, TestSuiteLab.class);
	}

	@Override
	public void setupCli(CliParser parser)
	{
		parser.addArgument(new Argument().withArgument("t").withLongName("min-strength"));
		parser.addArgument(new Argument().withArgument("t").withLongName("max-strength"));
		parser.addArgument(new Argument().withArgument("n").withLongName("max-iterations").withDescription("Sets the maximum number of iterations before the Greedy generator gives up"));
	}

	@Override
	public void setup()
	{	
		ArgumentMap map = getCliArguments();
		if (map.hasOption("max-strength"))
		{
			m_maxT = Integer.parseInt(map.getOptionValue("max-strength"));
		}
		if (map.hasOption("min-strength"))
		{
			m_minT = Integer.parseInt(map.getOptionValue("min-strength"));
		}
		if (map.hasOption("max-iterations"))
		{
			m_maxIterations = Integer.parseInt(map.getOptionValue("max-iterations"));
		}
		// Give a name and a description to the lab
		setTitle("Test sequence generation");
		setAuthor("Sylvain Hallé and Raphaël Khoury");
		setDescription(FileHelper.internalFileToString(this.getClass(), "pages/description.html"));

		// Create experiment groups and tables

		// Test suite size
		Group state_t_way_group = newGroup("State history");
		Group transition_t_way_group = newGroup("Transition history");
		Group ltl_group = newGroup("LTL-based triaging functions");
		Group card_group = newGroup("Equivalence class cardinality");
		ExperimentTable table_state_coverage_related = new ExperimentTable("Property", "Strength", TestSuiteGenerationExperiment.METHOD, "Size", "Length");
		table_state_coverage_related.setTitle("State coverage (with related work)");
		table_state_coverage_related.setNickname("StateCoverageRelated");
		table_state_coverage_related.setDescription("Size of generated test suites for various specifications also used in related works. These suites are generated to achieve 1-way state coverage.");
		add(table_state_coverage_related);
		ExperimentTable table_state_coverage_alone = new ExperimentTable("Property", "Strength", TestSuiteGenerationExperiment.METHOD, "Size", "Length");
		table_state_coverage_alone.setTitle("State coverage (no related work)");
		table_state_coverage_alone.setDescription("Size of generated test suites for various specifications, using the Cayley graph and greedy random techniques. These suites are generated to achieve <i>t</i>-way state coverage for various values of <i>t</i>.");
		table_state_coverage_alone.setNickname("StateCoverageAlone");
		add(table_state_coverage_alone);
		ExperimentTable table_transition_coverage_related = new ExperimentTable("Property", "Strength", TestSuiteGenerationExperiment.METHOD, "Size", "Length");
		table_transition_coverage_related.setTitle("Transition coverage (with related work)");
		table_transition_coverage_related.setNickname("TransitionCoverageRelated");
		table_transition_coverage_related.setDescription("Size of generated test suites for various specifications also used in related works. These suites are generated to achieve 1-way transition coverage.");
		add(table_transition_coverage_related);
		ExperimentTable table_transition_coverage_alone = new ExperimentTable("Property", "Strength", TestSuiteGenerationExperiment.METHOD, "Size", "Length");
		table_transition_coverage_alone.setTitle("Transition coverage (no related work)");
		table_transition_coverage_alone.setDescription("Size of generated test suites for various specifications, using the Cayley graph and greedy random techniques. These suites are generated to achieve <i>t</i>-way transition coverage for various values of <i>t</i>.");
		table_transition_coverage_alone.setNickname("TransitionCoverageAlone");
		add(table_transition_coverage_alone);
		// Holograms
		ExperimentTable ltl_table = new ExperimentTable("Formula", "Transformation", TestSuiteGenerationExperiment.METHOD, "Size", "Length", "Shortest", "Longest", "Coverage");
		ltl_table.setTitle("Hologram experiments");
		add(ltl_table);

		// Generation time with respect to strength
		ExperimentTable table_time_state_t_cayley = new ExperimentTable(CombinatorialTriagingFunctionProvider.STRENGTH, TestSuiteGenerationExperiment.METHOD, TestSuiteGenerationExperiment.DURATION);
		ExperimentTable table_time_state_t_greedy = new ExperimentTable(CombinatorialTriagingFunctionProvider.STRENGTH, TestSuiteGenerationExperiment.METHOD, TestSuiteGenerationExperiment.DURATION);
		TransformedTable table_time_state_t_cols = new TransformedTable(
				new Join(CombinatorialTriagingFunctionProvider.STRENGTH),
				new TransformedTable(
						new Composition(
								new GroupInColumns(CombinatorialTriagingFunctionProvider.STRENGTH, TestSuiteGenerationExperiment.DURATION),
								new BoxTransformation("Strength", "Cayley")),
								table_time_state_t_cayley),
								new TransformedTable(
										new Composition(
												new GroupInColumns(CombinatorialTriagingFunctionProvider.STRENGTH, TestSuiteGenerationExperiment.DURATION),
												new BoxTransformation("Strength", "Greedy")),
												table_time_state_t_greedy)
				);
		table_time_state_t_cols.setTitle("Generation time with respect to test strength (state coverage metric)");
		table_time_state_t_cols.setNickname("TimeVsStrengthState");
		add(table_time_state_t_cols);
		ExperimentTable table_time_transition_t = new ExperimentTable(CombinatorialTriagingFunctionProvider.STRENGTH, TestSuiteGenerationExperiment.METHOD, TestSuiteGenerationExperiment.DURATION);
		table_time_transition_t.setTitle("Generation time with respect to test strength (transition coverage metric)");
		table_time_transition_t.setNickname("TimeVsStrengthTransition");
		add(table_time_transition_t);

		// Test sequence size with respect to strength
		ExperimentTable table_size_state_t_cayley = new ExperimentTable(CombinatorialTriagingFunctionProvider.STRENGTH, TestSuiteGenerationExperiment.METHOD, TestSuiteGenerationExperiment.TOTAL_LENGTH);
		ExperimentTable table_size_state_t_greedy = new ExperimentTable(CombinatorialTriagingFunctionProvider.STRENGTH, TestSuiteGenerationExperiment.METHOD, TestSuiteGenerationExperiment.TOTAL_LENGTH);
		TransformedTable table_size_state_t_cols = new TransformedTable(
				new Join(CombinatorialTriagingFunctionProvider.STRENGTH),
				new TransformedTable(
						new Composition(
								new GroupInColumns(CombinatorialTriagingFunctionProvider.STRENGTH, TestSuiteGenerationExperiment.TOTAL_LENGTH),
								new BoxTransformation("Strength", "Cayley")),
								table_size_state_t_cayley),
								new TransformedTable(
										new Composition(
												new GroupInColumns(CombinatorialTriagingFunctionProvider.STRENGTH, TestSuiteGenerationExperiment.TOTAL_LENGTH),
												new BoxTransformation("Strength", "Greedy")),
												table_size_state_t_greedy)
				);
		table_size_state_t_cols.setTitle("Test sequence size with respect to test strength (state coverage metric)");
		table_size_state_t_cols.setNickname("SizeVsStrengthState");
		add(table_size_state_t_cols);
		ExperimentTable table_size_transition_t = new ExperimentTable(CombinatorialTriagingFunctionProvider.STRENGTH, TestSuiteGenerationExperiment.METHOD, TestSuiteGenerationExperiment.TOTAL_LENGTH);
		table_size_transition_t.setTitle("Test sequence size with respect to test strength (transition coverage metric)");
		table_size_transition_t.setNickname("SizeVsStrengthTransition");
		add(table_size_transition_t);

		// Greedy vs. Cayley
		VersusTable table_vs_size_state = new VersusTable(TestSuiteGenerationExperiment.TOTAL_LENGTH, "Cayley", "Greedy");
		table_vs_size_state.setTitle("Test suite size of Greedy vs. Cayley graph algorithms (state coverage metric)");
		table_vs_size_state.setNickname("SizeGreedyVsCayley");
		add(table_vs_size_state);
		VersusTable table_vs_time_state = new VersusTable(TestSuiteGenerationExperiment.DURATION, "Cayley", "Greedy");
		table_vs_time_state.setTitle("Generation time of Greedy vs. Cayley graph algorithms (state coverage metric)");
		table_vs_size_state.setNickname("TimeGreedyVsCayley");
		add(table_vs_time_state);

		// Greedy coverage
		ExperimentTable table_greedy_coverage_t = new ExperimentTable("Strength", "Coverage");
		TransformedTable table_greedy_coverage_t_box = new TransformedTable(
				new Composition(
						new GroupInColumns(CombinatorialTriagingFunctionProvider.STRENGTH, TestSuiteGenerationExperiment.COVERAGE),
						new BoxTransformation()),
						table_greedy_coverage_t);
		table_greedy_coverage_t_box.setTitle("Coverage of greedy solutions");
		table_greedy_coverage_t_box.setNickname("GreedyCoverage");
		add(table_greedy_coverage_t_box);

		// Greedy experiments with incomplete coverage
		GreedyIncompleteTable table_greedy_incomplete = new GreedyIncompleteTable(m_maxT);
		table_greedy_incomplete.setTitle("Number of greedy test suites with incomplete coverage");
		table_greedy_incomplete.setNickname("GreedyNumberIncomplete");
		add(table_greedy_incomplete);

		// Create a set of properties that are covered in related work
		Set<String> covered_properties_state = new HashSet<String>();
		Set<String> covered_properties_transitions = new HashSet<String>();

		// AUTOMATA-BASED EXPERIMENTS

		// Setup the write-in experiments
		{
			List<String> listing = FileHelper.getResourceListing(TestSuiteLab.class, s_writeInPath, ".*?\\.csv");
			ExperimentBuilder<TestSuiteWriteInExperiment> builder = new ExperimentBuilder<TestSuiteWriteInExperiment>(new TestSuiteWriteInExperiment());
			for (String uris : listing)
			{
				InputStream is = FileHelper.internalFileToStream(TestSuiteLab.class, "data/related/" + uris);
				Scanner scanner = new Scanner(is);
				Set<TestSuiteWriteInExperiment> experiments;
				try 
				{
					experiments = builder.buildExperiments(scanner);
					for (TestSuiteWriteInExperiment wie : experiments)
					{
						add(wie);
						String spec_name = wie.readString(AutomatonProvider.PROPERTY_NAME);
						if (wie.readString(CombinatorialTriagingFunctionProvider.FUNCTION).compareTo("State history") == 0)
						{
							state_t_way_group.add(wie);
							table_state_coverage_related.add(wie);
							covered_properties_state.add(spec_name);
						}	
						if (wie.readString(CombinatorialTriagingFunctionProvider.FUNCTION).compareTo("Transition history") == 0)
						{
							transition_t_way_group.add(wie);
							table_transition_coverage_related.add(wie);
							covered_properties_transitions.add(spec_name);
						}
					}
				}
				catch (ParseException e) 
				{
					e.printStackTrace();
				}
			}
		}

		// Setup the live experiments
		{
			List<String> listing = FileHelper.getResourceListing(TestSuiteLab.class, s_fsmPath, ".*?\\.dot");
			m_numAutomata = listing.size();
			for (String uris : listing)
			{
				for (int t = m_minT; t <= m_maxT; t++)
				{
					{
						// Cayley graph, state coverage
						TestSuiteGenerationExperiment c_e = addCayleyStateHistoryExperiment("data/fsm/" + uris, t);
						add(c_e);
						state_t_way_group.add(c_e);
						table_time_state_t_cayley.add(c_e);
						table_size_state_t_cayley.add(c_e);
						String spec_name = c_e.readString(AutomatonProvider.PROPERTY_NAME);
						if (covered_properties_state.contains(spec_name))
						{
							if (t == 1)
								table_state_coverage_related.add(c_e);
						}
						else
						{
							table_state_coverage_alone.add(c_e);
						}
						// Greedy, state coverage
						TestSuiteGenerationExperiment g_e = addGreedyStateHistoryExperiment("data/fsm/" + uris, t);
						add(g_e);
						state_t_way_group.add(g_e);
						table_time_state_t_greedy.add(g_e);
						table_size_state_t_greedy.add(g_e);
						table_greedy_coverage_t.add(g_e);
						table_greedy_incomplete.add(g_e, t);
						spec_name = g_e.readString(AutomatonProvider.PROPERTY_NAME);
						if (covered_properties_state.contains(spec_name))
						{
							if (t == 1)
								table_state_coverage_related.add(g_e);
						}
						else
						{
							table_state_coverage_alone.add(g_e);
						}
						table_vs_time_state.add(c_e, g_e);
						table_vs_size_state.add(c_e, g_e);
					}
					{
						// Cayley, transition coverage
						TestSuiteGenerationExperiment e = addCayleyTransitionHistoryExperiment("data/fsm/" + uris, t);
						add(e);
						transition_t_way_group.add(e);
						table_time_transition_t.add(e);
						table_size_transition_t.add(e);
						String spec_name = e.readString(AutomatonProvider.PROPERTY_NAME);
						if (covered_properties_transitions.contains(spec_name))
						{
							if (t == 1)
								table_transition_coverage_related.add(e);
						}
						else
						{
							table_transition_coverage_alone.add(e);
						}
					}
					{
						// Greedy, transition coverage
						TestSuiteGenerationExperiment e = addGreedyTransitionHistoryExperiment("data/fsm/" + uris, t);
						add(e);
						transition_t_way_group.add(e);
						table_time_transition_t.add(e);
						table_size_transition_t.add(e);
						table_greedy_coverage_t.add(e);
						table_greedy_incomplete.add(e, t);
						String spec_name = e.readString(AutomatonProvider.PROPERTY_NAME);
						if (covered_properties_transitions.contains(spec_name))
						{
							if (t == 1)
								table_transition_coverage_related.add(e);
						}
						else
						{
							table_transition_coverage_alone.add(e);
						}
					}
				}
			}
		}

		// LTL-BASED EXPERIMENTS
		{
			List<String> transformations = getHologramTransformations(s_ltlPath + "functions.txt");
			InputStream is = FileHelper.internalFileToStream(TestSuiteLab.class, s_ltlPath + "dwyer-ltl.txt");
			Scanner scanner = new Scanner(is);
			while (scanner.hasNextLine())
			{
				String line = scanner.nextLine().trim();
				if (line.isEmpty() || line.startsWith("#"))
				{
					continue;
				}
				for (String transformation : transformations)
				{
					{
						// Hologram
						TestSuiteGenerationExperiment exp = addHologramLtlExperiment(line, transformation);
						add(exp);
						ltl_table.add(exp);
						ltl_group.add(exp);
					}
					{
						// Greedy
						TestSuiteGenerationExperiment exp = addGreedyLtlExperiment(line, transformation);
						add(exp);
						ltl_table.add(exp);
						ltl_group.add(exp);
					}
				}
			}
			scanner.close();
		}

		// Distribution of equivalence classes: we compute this plot just
		// for one formula, to show what it looks like
		{
			CategoryDistributionExperiment e = getCategoryDistribution("G (a -> (X (b | c)))", "LD+FFD+RC10");
			add(e, card_group);
			ExperimentTable et = new ExperimentTable("Cardinality", "Number");
			et.add(e);
			add(et);
			//TransformedTable tt = new TransformedTable(new ExpandAsColumns("Cardinality", "Number"), et);
			//add(tt);
			//ClusteredHistogram histo = new ClusteredHistogram(et);
			Scatterplot histo = new Scatterplot(et);
			histo.setKey(false);
			histo.setNickname("PlotCategoryDistribution");
			add(histo);
		}

		// PLOTS
		{
			Scatterplot plot = new Scatterplot(table_vs_size_state);
			plot.setCaption(Axis.X, "Size (Cayley Graph)");
			plot.setCaption(Axis.Y, "Size (Greedy)");
			plot.withLines(false);
			plot.setNickname("PlotSizeGreedyVsCayley");
			add(plot);
		}
		{
			Scatterplot plot = new Scatterplot(table_vs_time_state);
			plot.setCaption(Axis.X, "Time (Cayley Graph)");
			plot.setCaption(Axis.Y, "Time (Greedy)");
			plot.withLines(false);
			plot.setNickname("PlotTimeGreedyVsCayley");
			add(plot);
		}
		{
			GnuBoxPlot plot = new GnuBoxPlot(table_time_state_t_cols, "Cayley", "Greedy");
			plot.setCaption(Axis.X, "Strength");
			plot.setCaption(Axis.Y, "Time (ms)");
			plot.setNickname("PlotBoxTimeGreedyVsCayley");
			add(plot);
		}
		{
			GnuBoxPlot plot = new GnuBoxPlot(table_size_state_t_cols, "Cayley", "Greedy");
			plot.setCaption(Axis.X, "Strength");
			plot.setCaption(Axis.Y, "Size");
			plot.setNickname("PlotBoxSizeGreedyVsCayley");
			add(plot);
		}
		{
			GnuBoxPlot plot = new GnuBoxPlot(table_greedy_coverage_t_box);
			plot.setCaption(Axis.X, "Strength");
			plot.setCaption(Axis.Y, "Coverage");
			plot.setNickname("PlotBoxGreedyCoverage");
			add(plot);
		}

		// MACROS
		{
			add(new LabStats(this));
		}
	}

	protected TestSuiteGenerationExperiment addCayleyStateHistoryExperiment(String filename, int strength)
	{
		InputStream is = FileHelper.internalFileToStream(TestSuiteLab.class, filename);
		Scanner scanner = new Scanner(is);
		AutomatonProvider ap = new AutomatonParser(scanner);
		TriagingFunctionProvider<AtomicEvent,MathList<Integer>> fp = new StateHistoryProvider(ap, strength);
		CayleyGraphProvider<AtomicEvent,MathList<Integer>> gp = new TriagingFunctionCayleyGraphProvider<AtomicEvent,MathList<Integer>>(fp);
		CayleyTraceGeneratorProvider<AtomicEvent,MathList<Integer>> ctgp = new CayleyClassCoverageGenerator<AtomicEvent,MathList<Integer>>(gp);
		TestSuiteProvider<AtomicEvent> tp = new CayleyTestSuiteGenerator<AtomicEvent,MathList<Integer>>(ctgp);
		TestSuiteGenerationExperiment exp = new LiveGenerationExperiment(tp);
		scanner.close();
		return exp;
	}

	protected TestSuiteGenerationExperiment addGreedyStateHistoryExperiment(String filename, int strength)
	{
		InputStream is = FileHelper.internalFileToStream(TestSuiteLab.class, filename);
		Scanner scanner = new Scanner(is);
		AutomatonProvider ap = new AutomatonParser(scanner);
		TriagingFunctionProvider<AtomicEvent,MathList<Integer>> fp = new StateHistoryProvider(ap, strength);
		CoverageMetricProvider<AtomicEvent,Float> cmp = new AutomatonCoverageMetricProvider<MathList<Integer>>(ap, fp);
		GreedyAutomatonGeneratorProvider ctgp = new GreedyAutomatonGeneratorProvider(ap, cmp);
		ctgp.setMaxIterations(m_maxIterations * strength);
		TestSuiteProvider<AtomicEvent> tp = new GreedyTestSuiteGenerator<AtomicEvent>(ctgp);
		GreedyTestSuiteGenerationExperiment exp = new GreedyTestSuiteGenerationExperiment(tp);
		scanner.close();
		return exp;
	}

	protected TestSuiteGenerationExperiment addCayleyTransitionHistoryExperiment(String filename, int strength)
	{
		InputStream is = FileHelper.internalFileToStream(TestSuiteLab.class, filename);
		Scanner scanner = new Scanner(is);
		AutomatonProvider ap = new AutomatonParser(scanner);
		TriagingFunctionProvider<AtomicEvent,MathList<Edge<AtomicEvent>>> fp = new TransitionHistoryProvider(ap, strength);
		CayleyGraphProvider<AtomicEvent,MathList<Edge<AtomicEvent>>> gp = new TriagingFunctionCayleyGraphProvider<AtomicEvent,MathList<Edge<AtomicEvent>>>(fp);
		CayleyTraceGeneratorProvider<AtomicEvent,MathList<Edge<AtomicEvent>>> ctgp = new CayleyClassCoverageGenerator<AtomicEvent,MathList<Edge<AtomicEvent>>>(gp);
		TestSuiteProvider<AtomicEvent> tp = new CayleyTestSuiteGenerator<AtomicEvent,MathList<Edge<AtomicEvent>>>(ctgp);
		TestSuiteGenerationExperiment exp = new LiveGenerationExperiment(tp);
		scanner.close();
		return exp;
	}

	protected TestSuiteGenerationExperiment addGreedyTransitionHistoryExperiment(String filename, int strength)
	{
		InputStream is = FileHelper.internalFileToStream(TestSuiteLab.class, filename);
		Scanner scanner = new Scanner(is);
		AutomatonProvider ap = new AutomatonParser(scanner);
		TriagingFunctionProvider<AtomicEvent,MathList<Edge<AtomicEvent>>> fp = new TransitionHistoryProvider(ap, strength);
		CoverageMetricProvider<AtomicEvent,Float> cmp = new AutomatonCoverageMetricProvider<MathList<Edge<AtomicEvent>>>(ap, fp);
		GreedyAutomatonGeneratorProvider ctgp = new GreedyAutomatonGeneratorProvider(ap, cmp);
		ctgp.setMaxIterations(m_maxIterations * strength);
		TestSuiteProvider<AtomicEvent> tp = new GreedyTestSuiteGenerator<AtomicEvent>(ctgp);
		GreedyTestSuiteGenerationExperiment exp = new GreedyTestSuiteGenerationExperiment(tp);
		scanner.close();
		return exp;
	}

	/**
	 * Creates an experiment that generates a trace using the Cayley Graph method
	 * @param formula The formula used as the specification
	 * @param transformation The string describing the hologram transformation
	 *   to apply
	 * @return The experiment
	 */
	protected TestSuiteGenerationExperiment addHologramLtlExperiment(String formula, String transformation)
	{
		OperatorProvider<AtomicEvent> op = new StringOperatorProvider(formula);
		HologramTransformationProvider<AtomicEvent> htp = new StringTransformationProvider(transformation);
		HologramTriagingFunctionProvider htfp = new HologramTriagingFunctionProvider(op, htp);
		CayleyGraphProvider<AtomicEvent,Operator<AtomicEvent>> gp = new FactoryCayleyGraphProvider<AtomicEvent,Operator<AtomicEvent>>(new AtomicLtlCayleyGraphFactory(), htfp);
		CayleyTraceGeneratorProvider<AtomicEvent,Operator<AtomicEvent>> ctgp = new CayleyClassCoverageGenerator<AtomicEvent,Operator<AtomicEvent>>(gp);
		TestSuiteProvider<AtomicEvent> tp = new CayleyTestSuiteGenerator<AtomicEvent,Operator<AtomicEvent>>(ctgp);
		TestSuiteGenerationExperiment exp = new LiveGenerationExperiment(tp);
		return exp;
	}

	/**
	 * Creates an experiment that generates a trace using the greedy random method
	 * @param formula The formula used as the specification
	 * @param transformation The string describing the hologram transformation
	 *   to apply
	 * @return The experiment
	 */
	protected TestSuiteGenerationExperiment addGreedyLtlExperiment(String formula, String transformation)
	{
		OperatorProvider<AtomicEvent> op = new StringOperatorProvider(formula);
		HologramTransformationProvider<AtomicEvent> htp = new StringTransformationProvider(transformation);
		HologramTriagingFunctionProvider htfp = new HologramTriagingFunctionProvider(op, htp);
		CoverageMetricProvider<AtomicEvent,Float> cmp = new HologramCoverageMetricProvider<MathList<Edge<AtomicEvent>>>(htfp);
		GreedyLtlGeneratorProvider ctgp = new GreedyLtlGeneratorProvider(op, cmp);
		ctgp.setMaxIterations(m_maxIterations);
		TestSuiteProvider<AtomicEvent> tp = new GreedyTestSuiteGenerator<AtomicEvent>(ctgp);
		GreedyTestSuiteGenerationExperiment exp = new GreedyTestSuiteGenerationExperiment(tp);
		return exp;
	}
	/**
	 * Creates a histogram showing how many categories there are of given
	 * cardinality
	 * @param parts
	 * @param max_len
	 */
	protected CategoryDistributionExperiment getCategoryDistribution(String formula, String transformation)
	{
		OperatorProvider<AtomicEvent> op = new StringOperatorProvider(formula);
		HologramTransformationProvider<AtomicEvent> htp = new StringTransformationProvider(transformation);
		HologramTriagingFunctionProvider htfp = new HologramTriagingFunctionProvider(op, htp);
		CayleyGraphProvider<AtomicEvent,Operator<AtomicEvent>> gp = new FactoryCayleyGraphProvider<AtomicEvent,Operator<AtomicEvent>>(new AtomicLtlCayleyGraphFactory(), htfp);
		CategoryDistributionExperiment cde = new CategoryDistributionExperiment(gp);
		return cde;
	}


	@Override
	public void setupCallbacks(List<WebCallback> callbacks)
	{
		callbacks.add(new FsmCallback(this, null));
		callbacks.add(new LtlCallback(this, null));
		callbacks.add(new GetAutomatonCallback(this, null));
		callbacks.add(new ShowAutomatonCallback(this, null));
	}

	public List<String> getHologramTransformations(String filename)
	{
		List<String> transfos = new ArrayList<String>();
		InputStream is = FileHelper.internalFileToStream(TestSuiteLab.class, filename);
		Scanner scanner = new Scanner(is);
		while (scanner.hasNextLine())
		{
			String line = scanner.nextLine().trim();
			if (line.isEmpty() || line.startsWith("#"))
			{
				continue;
			}
			transfos.add(line);
		}
		scanner.close();
		return transfos;
	}
}
